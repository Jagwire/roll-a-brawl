﻿using UnityEngine;
using System.Collections;

public class NewBehaviourScript : MonoBehaviour
{
    //public Text score;
    public double numScore;

    // Use this for initialization
    void Start()
    {
        var harvestSite = new Vector3(0, 0, 0);
        numScore = 0.0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseUp()
    {
        var harvestSite = Input.mousePosition;
        numScore = Mathf.Sqrt(harvestSite.x * harvestSite.x - (harvestSite.y + 1) * (harvestSite.y + 1));
    }
}