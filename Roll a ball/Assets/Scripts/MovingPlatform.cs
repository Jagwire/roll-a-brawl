﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class MovingPlatform : MonoBehaviour {
	
	public Vector3 directionExtent;
	public float moveSpeed = 1.0f;

	public GameObject player;

	private Vector3 originalPosition;
	private Vector3 directionToMove;


	void Awake() {
		player = GameObject.FindGameObjectWithTag ("Player");

		originalPosition = transform.position;
		
		directionToMove = directionExtent;// - originalPosition;
		directionToMove.Normalize();
		
	}
	// Update is called once per frame
	void Update () {
		
		transform.position += (directionToMove*Time.deltaTime*moveSpeed);
		
		if((transform.position - originalPosition).magnitude >= (directionExtent).magnitude) {
			directionToMove = -directionToMove;
		}
	}

}
