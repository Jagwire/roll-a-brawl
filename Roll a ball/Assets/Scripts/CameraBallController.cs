﻿using UnityEngine;
using System.Collections;

public enum DIRECTION
{
	FACING_FORWARD,
	FACING_RIGHT,
	FACING_BACKWARD,
	FACING_LEFT
}
public class CameraBallController : MonoBehaviour {


	public DIRECTION currentDirection;
	public GameObject player;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if (player != null) {
			transform.position = player.transform.position;
		}
	}

	public void RotateClockwise90(){
		transform.Rotate (0, 90, 0);
		switch(currentDirection) {
			case DIRECTION.FACING_FORWARD:
				currentDirection = DIRECTION.FACING_RIGHT;
				break;
			case DIRECTION.FACING_RIGHT:
				currentDirection = DIRECTION.FACING_BACKWARD;
				break;
			case DIRECTION.FACING_BACKWARD:
				currentDirection = DIRECTION.FACING_LEFT;
				break;
			case DIRECTION.FACING_LEFT:
				currentDirection = DIRECTION.FACING_FORWARD;
				break;
				
		}
	}
	public void RotateAntiClockwise90(){
		transform.Rotate(0,-90,0);
		switch(currentDirection) {
			case DIRECTION.FACING_FORWARD:
				currentDirection = DIRECTION.FACING_LEFT;
				break;
			case DIRECTION.FACING_RIGHT:
				currentDirection = DIRECTION.FACING_FORWARD;
				break;
			case DIRECTION.FACING_BACKWARD:
				currentDirection = DIRECTION.FACING_RIGHT;
				break;
			case DIRECTION.FACING_LEFT:
				currentDirection = DIRECTION.FACING_BACKWARD;
				break;
				
		}
	}
}
