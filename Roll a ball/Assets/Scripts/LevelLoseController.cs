﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LevelLoseController : MonoBehaviour {

	public Button tryAgainButton;
	public Button levelSelectButton;

	void Awake() {
		tryAgainButton.onClick.AddListener(() => {
			Application.LoadLevel(Application.loadedLevel);
		});
		
		levelSelectButton.onClick.AddListener(() => {
			Application.LoadLevel("LevelSelectScene");
		});
	}
}
