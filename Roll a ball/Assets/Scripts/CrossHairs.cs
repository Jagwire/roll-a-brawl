﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
public class CrossHairs : MonoBehaviour {

	private bool showing = false;
	private GameObject target;
	// Use this for initialization
	void Start () {
	
	}
	void Awake() {
		GetComponent<Image>().color = new Color(0,0,0,0);
		
		
		TargetingSystem ts = TargetingSystem.Instance;
		Debug.LogError("SETTING UP TARGET HANDLERS!");
		ts.targeted += targetedCallback;
		
		ts.untargeted += untargetedCallback;
	}
	
	void OnDestroy() {
		TargetingSystem ts = TargetingSystem.Instance;
		ts.targeted -= targetedCallback;
		ts.untargeted -= untargetedCallback;
	}
	
	public void targetedCallback(GameObject go, EventArgs e) {
		Debug.LogError("TARGETED FROM CROSSHAIRS!!!");
			target = go;
			showCrossHairs();
	}
	
	public void untargetedCallback(GameObject go, EventArgs e) {
		Debug.LogError("UNTARGETED!");
	}
	
	
	void showCrossHairs() {
		showing = true;
		GetComponent<Image>().color = new Color(1,1,1,1);
	}
	
	void hideCrossHairs() {
		GetComponent<Image>().color = new Color(0,0,0,0);
		showing = false;
	}
	
	
	// Update is called once per frame
	void Update () {
		if(showing) {
			if(target == null) {
				hideCrossHairs();
				return;
			}
			
			Vector3 anchoredPosition = Camera.main.WorldToScreenPoint(target.transform.position);
			GetComponent<RectTransform>().anchoredPosition = anchoredPosition;
		}
	}
}
