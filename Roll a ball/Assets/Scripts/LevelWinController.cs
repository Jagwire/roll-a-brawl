﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LevelWinController : MonoBehaviour {
	public Button continueButton;
	public Button levelSelectButton;
	
	void Awake() {
		levelSelectButton.onClick.AddListener(() => {
			Application.LoadLevel("LevelSelectScene");
		});
		
		continueButton.onClick.AddListener(() => {
			Debug.LogError("TODO: load next level");	
		});
	}
}
