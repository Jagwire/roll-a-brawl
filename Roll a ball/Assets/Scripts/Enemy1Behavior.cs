﻿using UnityEngine;
using System.Collections;

public class Enemy1Behavior : MonoBehaviour {

	public float secondsBeforeDelete = 2.0f;
	private float timeOfDeath = 0.0f;
	private bool dead = false;
	void OnTriggerEnter() {
		if(dead) { return; }
		
		
		timeOfDeath = Time.realtimeSinceStartup;
		Vector3 currentScale = transform.localScale;
		Vector3 currentPosition = transform.position;
		transform.localScale = new Vector3(currentScale.x, 0.0001f, currentScale.z);
		transform.position = new Vector3(currentPosition.x, 0.0001f/2.0f, currentPosition.z);
		
		GetComponent<Rotator>().spinSpeed = 0.0f;
		
		dead = true;
// 		Destroy(this.gameObject);
	}
	
	void Update() {
		if(dead) {
			if(timeOfDeath+secondsBeforeDelete <= Time.realtimeSinceStartup) {
				Destroy(this.gameObject);
			} 
		}
	}
}
