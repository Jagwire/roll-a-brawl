﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

	public enum RotationAxis {cubeRotation,pyramidRotation};
	public RotationAxis rot;
	// Update is called once per frame
	public float spinSpeed = 1.0f;
	void Update () 
	{
		if (rot == RotationAxis.cubeRotation) {
			transform.Rotate (new Vector3 (15, 30, 45) * Time.deltaTime);
		}
		if (rot == RotationAxis.pyramidRotation) {
			transform.Rotate (new Vector3(0,60,0) * Time.deltaTime*spinSpeed);
		}
	}
}
