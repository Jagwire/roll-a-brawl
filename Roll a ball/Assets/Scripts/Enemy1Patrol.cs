﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Enemy1Patrol : MonoBehaviour {

	public Vector3 directionExtent;
	public float moveSpeed = 1.0f;

	private Vector3 originalPosition;
	private Vector3 directionToMove;
	void Awake() {
		originalPosition = transform.position;
		
		directionToMove = directionExtent;// - originalPosition;
		directionToMove.Normalize();
		
	}
	// Update is called once per frame
	void Update () {
		
		transform.position += (directionToMove*Time.deltaTime*moveSpeed);
		
		if((transform.position - originalPosition).magnitude >= (directionExtent).magnitude) {
			directionToMove = -directionToMove;
		}
	}
	
	void OnTriggerEnter(Collider other) {
		Destroy(this.gameObject);
	}
}
