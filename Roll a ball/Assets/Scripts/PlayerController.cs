﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour 
{
	public float speed;
	private int count;
	public Text scoreText;
	public Text winText;

	public CameraBallController cameraBallController;

	void Start () 
	{
		count = 0;
		setScoreText();
		winText.text = "";
		UpdateHealthUI ();
	}
	
	private Vector3 initial;
	void Awake() {
		if(Application.platform == RuntimePlatform.IPhonePlayer) {
			initial = Input.acceleration;
		}
	}
	public float dieDepth = -50f;
	void FixedUpdate ()
	{
		
		if(transform.position.y <= dieDepth ) {
			//lose level
			//TODO: we should actually show some kind of window here instead of just
			//loading a level
			// Maybe: set player to kinematic and show  a loss panel
// 			Application.LoadLevel("LevelSelectScene");
			showLosePanel();
			Destroy(this.gameObject);
		}
		
		if(Application.platform != RuntimePlatform.IPhonePlayer) {
			float moveHorizontal = Input.GetAxis("Horizontal");
			float moveVertical = Input.GetAxis("Vertical");
			
			Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
			if(!Input.GetKeyUp(KeyCode.Space)) {
				GetComponent<Rigidbody>().AddForce(movement*10*Time.deltaTime, ForceMode.Impulse);
			} else {
				GetComponent<PlayerAttackController>().dash();
			}
			return;	
		}
		
		DIRECTION currentDir = cameraBallController.currentDirection;
		
		Vector3 dir = Vector3.zero;
		
		switch(currentDir) {
			case DIRECTION.FACING_FORWARD:
				dir.x = Input.acceleration.x-initial.x;
				dir.z = Input.acceleration.y-initial.y;
				break;
			case DIRECTION.FACING_RIGHT:
				dir.x = Input.acceleration.y-initial.y;
				dir.z = -Input.acceleration.x-initial.x;
				break;
			case DIRECTION.FACING_BACKWARD:
				dir.x = -Input.acceleration.x-initial.x;
				dir.z = -Input.acceleration.y-initial.y;
				break;
			case DIRECTION.FACING_LEFT:
				dir.x = -Input.acceleration.y-initial.y;
				dir.z = Input.acceleration.x-initial.x;
				break;	
		}

		dir.y = 0.0f;
		dir *= Time.deltaTime;
		GetComponent<Rigidbody>().AddForce(dir*speed);
	}

	int score = 0;
	void OnTriggerEnter(Collider other)
	{
		//Destroy (other.gameObject);
		if (other.gameObject.tag == "PickUp") 
		{
			other.gameObject.SetActive (false);
			count += 1;
			setScoreText();
		}
		if (other.gameObject.tag == "Pyramid") {
			
			if(isDashing()) {
				//kill pyramid
				//Destroy(other.gameObject);
			} else {
				//kill player, end game
				Destroy(this.gameObject);
				showLosePanel();
			}
		}
		if (other.gameObject.tag == "Cylinder") {

		}
		if(other.gameObject.tag == "Goal") {
			//WIN!
			Debug.LogError("WIN!");
			GetComponent<Rigidbody>().isKinematic = true;
			showWinPanel();
		}
	}

	public GameObject LosePanel;
	public void showLosePanel() {
		LosePanel.active = true;
// 		GetComponent<Rigidbody>().isKine
	}
	public GameObject WinPanel;
	public void showWinPanel() {
		Debug.LogError("SHOWING WIN PANEL!");
		WinPanel.SetActive(true);
	}
	
	public GameObject PausedPanel;
	private bool isPaused = false;
	public void togglePaused() {
		isPaused = !isPaused;
		PausedPanel.SetActive(isPaused);
		GetComponent<Rigidbody>().isKinematic = isPaused;
	}
	
	public void resumePlay() {
		isPaused = false;
		PausedPanel.SetActive(false);
		GetComponent<Rigidbody>().isKinematic = false;
	}
	
	
	void setScoreText()
	{
		scoreText.text = ""+count;
	}

	public void UpdateHealthUI(){
	}
	
	private bool isDashing() {
		return GetComponent<PlayerAttackController>().isDashing;
	}
}
