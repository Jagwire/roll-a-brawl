﻿using UnityEngine;
using System.Collections;

public class PlatformPlayerContainer : MonoBehaviour {

	public GameObject movingPlatform;
	// Use this for initialization
	void Start () {
		transform.localScale = new Vector3 ((1 / movingPlatform.transform.localScale.x), (1 / movingPlatform.transform.localScale.y), (1 / movingPlatform.transform.localScale.z));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
