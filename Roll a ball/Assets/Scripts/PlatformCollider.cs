﻿using UnityEngine;
using System.Collections;

public class PlatformCollider : MonoBehaviour {

	public GameObject playerContainer;
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			other.transform.SetParent(playerContainer.transform);
			other.transform.localScale = new Vector3(1,1,1);
		} 
	}
	void OnTriggerExit(Collider other){
		if (other.tag == "Player") {
			other.transform.SetParent(null);
			other.transform.localScale = new Vector3(1,1,1);
		}
	}
}
