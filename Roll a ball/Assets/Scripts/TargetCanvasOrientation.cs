﻿using UnityEngine;
using System.Collections;

public class TargetCanvasOrientation : MonoBehaviour {

	public GameObject target;
	public bool isCameraRotation = true;
	
	// Update is called once per frame
	void Update () {
		Vector3 tempPos = target.transform.position;
		transform.position = new Vector3(tempPos.x, tempPos.y+0.5f+target.transform.localScale.x/2, tempPos.z);
		if(isCameraRotation) {
			transform.rotation = Camera.main.transform.rotation;
		}
		
		
	}
}
