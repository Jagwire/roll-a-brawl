﻿using UnityEngine;
using System.Collections;

public class Targetable : MonoBehaviour {

	void Awake() {
		TargetingSystem.Instance.addTargetable(this.gameObject);
	}
	
	
	void OnMouseUpAsButton() {
		Debug.LogError("TARGETING: "+this.gameObject.GetInstanceID());
		TargetingSystem.Instance.currentTargetIndex = this.gameObject.GetInstanceID();
	}
}
