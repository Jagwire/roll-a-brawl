﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Boss1Behavior : MonoBehaviour {

	public enum STATE {
		INIT,
		ATTACKING,
		RAGE
	}
	
	public GameObject rotator;
	public GameObject mover;
	public float bossMoveSpeed = 3.0f;
	public float bossSpinSpeed = 1.0f;
	private float timeOfCollision = 10000000f;
	private float timeUntilKinematic = 1.5f;
	private STATE currentState = STATE.INIT;
	private GameObject player;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 direction = Vector3.up;
		
		if(healthBar.value <= 0) {
			Die();
			return;
		}		
		
		switch(currentState) {
			case STATE.INIT:
				if(Time.realtimeSinceStartup > timeOfCollision+timeUntilKinematic) {
					GetComponent<Rigidbody>().isKinematic = true;
					currentState = STATE.ATTACKING;
					player = GameObject.FindGameObjectWithTag("Player");
					if(player == null) { Debug.LogError("PROBLEMS!!!");}
				}
				break;
			
			case STATE.ATTACKING:
				if(healthBar.value < 30) {
					currentState = STATE.RAGE;
					bossSpinSpeed *= 10f;
					return;
				}
				rotator.transform.Rotate(Vector3.up, bossSpinSpeed);
				direction = player.transform.position - transform.position;
				direction.y = 0.0f;
				direction.Normalize();
				mover.transform.Translate(direction*Time.deltaTime*bossMoveSpeed);
				break;
			case STATE.RAGE:
				//change color
				GetComponent<Renderer>().material.color = Color.red;
				//spin
				rotator.transform.Rotate(Vector3.up, bossSpinSpeed);
				direction = player.transform.position - transform.position;
				direction.y = 0.0f;
				direction.Normalize();
				mover.transform.Translate(direction*Time.deltaTime*bossMoveSpeed);
				break;
			
			
		}
	}
	
	public Slider healthBar;
	public float collisionDamage;
	void OnCollisionEnter(Collision other) {
		switch(currentState) {		
			case STATE.INIT:
				timeOfCollision = Time.realtimeSinceStartup;
				Debug.Log("COLLISION!");
				break;
			case STATE.ATTACKING:
				Debug.Log("OUCH!");
				healthBar.value -= collisionDamage;
				break;
			case STATE.RAGE:
				Debug.Log("RAGE OUCH!");
				healthBar.value -= collisionDamage;
				break;
			
		}
	}
	
	private void Die() {
		
		GameObject.FindGameObjectWithTag("Player");
		//destroy health bar?
		
		//show win panel!
		Debug.LogError("DEATH!!!!!!");
		Destroy(this.gameObject);
	}
}
