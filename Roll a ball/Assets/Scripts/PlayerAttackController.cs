﻿using UnityEngine;
using System.Collections;

public class PlayerAttackController : MonoBehaviour {
	public GameObject innerSphere;
	public int count = 0;
	public float resizeSpeed = 2;
	public bool isShrink = false;
	public float isShrinkTimer = 0.0f;
	public float SHRINK_TIMER_THRESHOLD = 200f;
	public float MINIMUM_SIZE_THRESHOLD = 0.3f;
	private float REGULAR_SIZE_THRESHOLD = 1.0f;
	public bool canShrink = true;
	public CameraBallController cbc;
	private DIRECTION currentDir;
	private Vector3 initial;
	void Awake() {
		if(Application.platform == RuntimePlatform.IPhonePlayer) {
			initial = Input.acceleration;
		}
	}
	
	public void dash() {
		charge();
	}
	public bool isDashing = false;
	private float elapsedDashingTime = 0.0f;
	public void charge() {
		Vector3 dir = Vector3.zero;
		GameObject target = TargetingSystem.Instance.CurrentTarget;
		float dashForce = 900.0f;
		
		if(target != null) {
			Debug.LogError("DASHING TOWARD TARGET!");
			dir = target.transform.position - transform.position;
			dir.Normalize();
			dashForce = 1800.0f;
		}
		else if(Application.platform != RuntimePlatform.IPhonePlayer) {
			float moveHorizontal = Input.GetAxis("Horizontal");
			float moveVertical = Input.GetAxis("Vertical");
			
			dir = new Vector3(moveHorizontal, 0.0f, moveVertical);
			dir.Normalize();
			dir.y = 0.0f;

		} else {
			currentDir = cbc.currentDirection;
			switch(currentDir) {
					case DIRECTION.FACING_FORWARD:
						dir.x = Input.acceleration.x-initial.x;
						dir.z = Input.acceleration.y-initial.y;
						break;
					case DIRECTION.FACING_RIGHT:
						dir.x = Input.acceleration.y-initial.y;
						dir.z = -Input.acceleration.x-initial.x;
						break;
					case DIRECTION.FACING_BACKWARD:
						dir.x = -Input.acceleration.x-initial.x;
						dir.z = -Input.acceleration.y-initial.y;
						break;
					case DIRECTION.FACING_LEFT:
						dir.x = -Input.acceleration.y-initial.y;
						dir.z = Input.acceleration.x-initial.x;
						break;	
				}
				dir.y = 0.0f;
		}

		
		dir *= Time.deltaTime;
		GetComponent<Rigidbody>().AddForce(dir*dashForce, ForceMode.Impulse);
		isDashing = true;
		elapsedDashingTime = 0.0f;
	}


	public void ToggleShrink(){
		if (canShrink) {
			isShrink = !isShrink;

			if (isShrink) {
				canShrink = false;
			}
		}
	}
	void SetSizeToDefault(){
		innerSphere.transform.localScale = new Vector3 (REGULAR_SIZE_THRESHOLD, REGULAR_SIZE_THRESHOLD, REGULAR_SIZE_THRESHOLD);
	}
	void SetSizeOfBall(){
		if (isShrink) {
			isShrinkTimer +=1;

			if (innerSphere.transform.localScale.x > MINIMUM_SIZE_THRESHOLD){
				innerSphere.transform.localScale = new Vector3(innerSphere.transform.localScale.x - (resizeSpeed*2) * Time.deltaTime, 
				                                               innerSphere.transform.localScale.y - (resizeSpeed*2) * Time.deltaTime,
				                                               innerSphere.transform.localScale.x - (resizeSpeed*2) * Time.deltaTime);
			}

			if (isShrinkTimer >= SHRINK_TIMER_THRESHOLD){
				canShrink = true;
				ToggleShrink();
				isShrinkTimer = 0;
			}
		} 
		else if (!isShrink){
			if (innerSphere.transform.localScale.x < REGULAR_SIZE_THRESHOLD){
				innerSphere.transform.localScale = new Vector3(innerSphere.transform.localScale.x + resizeSpeed * Time.deltaTime, 
				                                               innerSphere.transform.localScale.y + resizeSpeed * Time.deltaTime,
				                                               innerSphere.transform.localScale.x + resizeSpeed * Time.deltaTime);
			} else {
				canShrink = true;
			}
		}
	}
	
	public float maximumDashingTime = 1.0f;
	// Update is called once per frame
	void Update () {
		if(elapsedDashingTime >= maximumDashingTime) {
			isDashing = false;
		}
		
		if(isDashing) {
			elapsedDashingTime += Time.deltaTime;
		}
		SetSizeOfBall ();
	}
}