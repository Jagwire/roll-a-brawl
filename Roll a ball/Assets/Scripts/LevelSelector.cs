﻿using UnityEngine;
using System.Collections;

public class LevelSelector : MonoBehaviour {
	public void loadlWorld1Level1() {
		Application.LoadLevel("world1level1");
	}
	
	public void loadWorld1Level2() {
		Application.LoadLevel("world1level2");
	}
	
	public void loadWorld1Level3() {
		Application.LoadLevel("world1level3");
	}
	
	public void loadWorld1Level4() {
		Application.LoadLevel("world1level4");
	}
	
	public void loadWorld1Level5() {
		Application.LoadLevel("world1level5");
	}
	
	public void loadWorld1(string level) {
		Application.LoadLevel("world1level"+level);
	}
	
	public void loadWorld2(string level) {
		Application.LoadLevel("world2level"+level);
	}
	
	public void loadWorld3(string level) {
		Application.LoadLevel("world3level"+level);
	}
	
	public void loadShittyLevel() {
		Application.LoadLevel("test_bed");
	}
}
