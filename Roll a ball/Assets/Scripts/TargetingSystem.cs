﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class TargetingSystem {

	public delegate void TargetChangedHandler(GameObject target, EventArgs e);
	public event TargetChangedHandler targeted, untargeted;

	private IDictionary<int, GameObject> targetables;
	private static TargetingSystem _instance = null;
	protected TargetingSystem() {
		targetables = new Dictionary<int, GameObject>();
	}
	
	public static TargetingSystem Instance {
		get {
			if(_instance == null) {
				_instance = new TargetingSystem();
			}
			return _instance;
		}
	}
	
	private int _currentTargetIndex = -1;
	public int currentTargetIndex 
	{
		get {
			return _currentTargetIndex;
		}
		set {
			if(_currentTargetIndex != -1) {
				untargeted(CurrentTarget, null);
			} 
			
			_currentTargetIndex = value;
			if(value != -1) {
				targeted(CurrentTarget, null);
			}
		}
	}
	public GameObject CurrentTarget {
		get {
			if(_currentTargetIndex == -1) { return null; }
			
			return targetables[_currentTargetIndex];
		}
	}
	
	public void  addTargetable(GameObject obj) {
		targetables[obj.GetInstanceID()] = obj;
	}
	
	public void removeTargetable(GameObject obj) {
		targetables.Remove(obj.GetInstanceID());
	}
	
	public void untarget() {
		_currentTargetIndex = -1;
	}
	
}
