﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LevelPauseController : MonoBehaviour {

	
	public PlayerController player;
	public Button unpauseButton;
	public Button levelSelectButton;
	public Button restartButton;
	void Awake() {
		unpauseButton.onClick.AddListener(() => {
			player.resumePlay();
		});
		
		levelSelectButton.onClick.AddListener(() => {
			Application.LoadLevel("LevelSelectScene");
		});
		
		restartButton.onClick.AddListener(() => {
			Application.LoadLevel(Application.loadedLevelName);
		});
	}
}
