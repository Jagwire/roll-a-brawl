﻿using UnityEngine;
using System.Collections;

public class PlayerCanvasOrientation : MonoBehaviour {

	GameObject player;
	public bool isCameraRotation = false;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("InnerPlayer");
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 tempPos = player.transform.position;
		transform.position = new Vector3 (tempPos.x, tempPos.y + 0.5f + player.transform.localScale.x / 2, tempPos.z);
		if (isCameraRotation) {
			transform.rotation = Camera.main.transform.rotation;
		}
	}
}
